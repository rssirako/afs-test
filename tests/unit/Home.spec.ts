import { shallowMount, createLocalVue } from '@vue/test-utils';
import Home from 'views/Home.vue';
import Buefy from "buefy";

const BTableStub = {
  template: '<span class="b-table-stub"></span>'
};

describe('Home', () => {
  it('renders a h1', () => {
    // To Do- use beforeEach to get rid of repetative code
    const localVue = createLocalVue();
    localVue.use(Buefy);
    const wrapper = shallowMount(Home, {
      localVue,
    })
    expect(wrapper.find('h1').text()).toMatch('This is a table with some important data');
  })
  it('should render a table', async () => {
    const localVue = createLocalVue();
    localVue.use(Buefy);
    const wrapper = shallowMount(Home, {
      localVue,
      stubs: {
        'b-table': BTableStub,  
      }
    })
    const vm = wrapper.vm
    
    await vm.$nextTick();
    await vm.$nextTick();
  
    expect(wrapper.find('span').attributes().columns).toBeTruthy()
  })
})
