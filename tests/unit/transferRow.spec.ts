import { shallowMount, createLocalVue } from '@vue/test-utils';
import TransferRow from 'components/TransferRow.vue';



describe('TransferRow', () => {
  it('shows table row data', () => {
    const localVue = createLocalVue();
    const wrapper = shallowMount(TransferRow, {
      propsData: {
        transfer: {
          splitFactor: null,
          exDate: null,
          amount: 200,
          companyId: "568fa387-43d1-499a-bba2-25089f5a881a",
          notes: null,
          pricePerShare: null,
          recordDate: "2021-07-01",
          securityClassId: "ab983cfe-a932-4e25-98ea-f5928a839fe1",
          securityClass: { name: "Common" },
          state: "NEW",
          toSecurityHolderId: null,
          toSecurityHolder: null,
          transactionIdentifier: "ec485c74-713c-486d-9ecb-1099d2862074",
          positionWithinDay: 1,
          type: "INCREASE_AUTHORIZED_CAPITAL",
        }
      },
      localVue,
    })

    expect(wrapper.find('.transfer-row__type').text()).toMatch('Type: INCREASE_AUTHORIZED_CAPITAL');
    expect(wrapper.find('.transfer-row__state').text()).toMatch('State: NEW');
    expect(wrapper.find('.transfer-row__amount').text()).toMatch('Amount: 200');
  })
})
